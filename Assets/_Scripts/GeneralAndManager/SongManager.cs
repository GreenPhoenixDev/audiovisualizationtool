﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class SongManager : MonoBehaviour
{
	#region Private Variables
	[SerializeField] AudioSource secondSource;
	[SerializeField] TMP_InputField folderInput;
	[SerializeField] TMP_Text songName;

	List<AudioClip> songs = new List<AudioClip>();

	AudioSource source;
	SampleRateManager sRM;
	string songFolder;
	int curSongIdx;
	bool isStopped;
	bool isPaused;
	bool startedList;
	#endregion

	#region MonoBehaviour
	//void Awake()
	//{
	//	// if the song folder does not exist, create or fill it
	//	if (SongFolder == null || SongFolder.Length == 0)
	//	{
	//		SongFolder = Application.dataPath + "/" + audioFolderName;

	//		Directory.CreateDirectory(SongFolder);
	//	}

	//	UpdateAudioFiles();
	//}
	void Start()
	{
		source = GetComponent<AudioSource>();
		sRM = GetComponent<SampleRateManager>();
		isStopped = true;

		folderInput.text = PlayerPrefs.GetString("SongFolder");
		songFolder = folderInput.text;
		UpdateAudioFiles();
	}
	void Update()
	{
		if (source.clip != null)
			if (source.time >= source.clip.length)
				NextSong();
	}
	#endregion
	#region Public Methods
	public void UpdateAudioFiles()
	{
		if (songFolder == null || songFolder.Length == 0) { return; }

		// get all audio files and load them into a list
		string[] audioFiles = Directory.GetFiles(songFolder);

		if (audioFiles.Length == 0) { songName.text = "No songs found!"; }

		for (int i = 0; i < audioFiles.Length; i++)
		{
			// add .aif, .wav, .mp3, .ogg files only
			string[] line = audioFiles[i].Split('.');
			if (line[line.Length - 1] == "wav" || line[line.Length - 1] == "mp3" ||
				line[line.Length - 1] == "ogg")
			{
				string[] parts = line[0].Split('/');
				string[] parts2 = parts[parts.Length - 1].Split('\\');

				StartCoroutine(LoadSongIntoList(audioFiles[i], parts2[parts2.Length - 1]));
			}
		}
	}

	void OnApplicationQuit()
	{
		PlayerPrefs.SetString("SongFolder", folderInput.text);
		PlayerPrefs.Save();
	}
	// Buttons
	public void PlayMusic()
	{
		if (songs.Count == 0) { return; }
		if (!startedList)
		{
			source.clip = songs[0];
			startedList = true;
		}

		if (isStopped || isPaused) { PlayHelper(); }
	}
	public void PauseMusic()
	{
		if (isStopped || isPaused) { return; }

		PauseHelper();
	}
	public void StopMusic()
	{
		if (!isStopped) { StopHelper(); }
	}
	public void NextSong()
	{
		if (songs.Count == 0) { return; }

		if (curSongIdx == songs.Count - 1) { curSongIdx = 0; }
		else { curSongIdx++; }

		source.clip = songs[curSongIdx];
		secondSource.clip = songs[curSongIdx];
		UpdateSongText();

		PlayHelper();
	}
	public void PreviousSong()
	{
		if (songs.Count == 0) { return; }

		if (curSongIdx == 0) { curSongIdx = songs.Count - 1; }
		else { curSongIdx--; }

		source.clip = songs[curSongIdx];
		secondSource.clip = songs[curSongIdx];
		UpdateSongText();

		PlayHelper();
	}
	public void ChangeSongFolder()
	{
		StopHelper();

		// change to forward slashes
		songFolder = folderInput.text;
		songFolder = songFolder.Replace(@"\", "/");
		songFolder += "/";
		startedList = false;

		UpdateAudioFiles();
	}
	#endregion
	#region Little Helper
	void PlayHelper()
	{
		sRM.ListenToAudio = true;
		source.Play();
		secondSource.Play();
		isStopped = false;
		isPaused = false;
	}
	void PauseHelper()
	{
		sRM.ListenToAudio = false;
		source.Pause();
		secondSource.Pause();
		isPaused = true;
	}
	void StopHelper()
	{
		sRM.ClearArrays();
		sRM.ListenToAudio = false;
		source.Stop();
		secondSource.Stop();
		isStopped = true;
	}
	void UpdateSongText()
	{
		songName.text = songs[curSongIdx].name;
	}
	#endregion

	#region Coroutines
	IEnumerator LoadSongIntoList(string filename, string songname)
	{
		WWW www = new WWW(filename);

		yield return www;

		if (www.error != null) { Debug.Log(www.error); }

		AudioClip newClip = www.GetAudioClip();
		newClip.LoadAudioData();
		newClip.name = songname;

		while (newClip.loadState == AudioDataLoadState.Loading || newClip.loadState == AudioDataLoadState.Unloaded)
		{
			yield return null;
		}

		if (newClip.loadState == AudioDataLoadState.Failed) { Debug.LogError($"Could not load audiofile {songname} at {filename}"); }
		else { songs.Add(newClip); }

		if (songName.text.Length != 0) { songName.text = songs[0].name; }
	}
	#endregion
}