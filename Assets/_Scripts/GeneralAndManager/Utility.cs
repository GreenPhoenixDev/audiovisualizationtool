﻿using UnityEngine;

public class Utility : MonoBehaviour
{
	#region FactorsOfTwo
	public static int SwitchFactorsOfTwoFull(FactorsOfTwo switchFactor)
	{
		int value = 0;

		switch (switchFactor)
		{
			case FactorsOfTwo._2:
				value = 2;
				break;
			case FactorsOfTwo._4:
				value = 4;
				break;
			case FactorsOfTwo._8:
				value = 8;
				break;
			case FactorsOfTwo._16:
				value = 16;
				break;
			case FactorsOfTwo._32:
				value = 32;
				break;
			case FactorsOfTwo._64:
				value = 64;
				break;
			case FactorsOfTwo._128:
				value = 128;
				break;
			case FactorsOfTwo._256:
				value = 256;
				break;
			case FactorsOfTwo._512:
				value = 512;
				break;
			default:
				break;
		}
		return value;
	}
	public static int SwitchFactorsOfTwoMappedToLength(FactorsOfTwo switchFactor)
	{
		int value = 0;

		switch (switchFactor)
		{
			case FactorsOfTwo._2:
				value = 0;
				break;
			case FactorsOfTwo._4:
				value = 1;
				break;
			case FactorsOfTwo._8:
				value = 2;
				break;
			case FactorsOfTwo._16:
				value = 3;
				break;
			case FactorsOfTwo._32:
				value = 4;
				break;
			case FactorsOfTwo._64:
				value = 5;
				break;
			case FactorsOfTwo._128:
				value = 6;
				break;
			case FactorsOfTwo._256:
				value = 7;
				break;
			case FactorsOfTwo._512:
				value = 8;
				break;
			default:
				break;
		}
		return value;
	}
	#endregion
}
#region Enums
public enum FactorsOfTwo { _2, _4, _8, _16, _32, _64, _128, _256, _512 }
public enum VisualizationState { FrequencyBands, BandBuffer, AudioBands, AudioBandBuffer }
public enum Channel { Stereo, Left, Right }
public enum ScreenSize { _1280x720, _1280x800, _1600x900, _1920x1080, _2048x1152, _2560x1440, _3840x2160 }
public enum ScreenMode { FullScreen, Windowed }
#endregion