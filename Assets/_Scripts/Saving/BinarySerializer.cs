﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class BinarySerializer : MonoBehaviour
{
	#region Save And Load
	public void SaveGameData(string path, string filename, SaveData data)
	{
		if(File.Exists(path + filename))
		{
			Debug.LogError("Overriding File!");
		}

		using(FileStream stream = File.Create(path + filename))
		{
			BinaryFormatter bf = new BinaryFormatter();
			bf.Serialize(stream, data);
		}
	}
	public SaveData LoadGameData(string path, string filename)
	{
		if(!File.Exists(path + filename))
		{
			Debug.LogError("File does not exist!");
			return null;
		}
		else
		{
			using(FileStream stream = new FileStream(path + filename, FileMode.Open))
			{
				BinaryFormatter bf = new BinaryFormatter();
				return (SaveData)bf.Deserialize(stream);
			}
		}
	}
	#endregion
}

#region Serializable Classes
[System.Serializable]
public class SaveData
{
	public List<SaveItem> SaveItemList = new List<SaveItem>();

	public SaveData(List<SaveItem> items)
	{
		SaveItemList = items;
	}
}
[System.Serializable]
public class SaveItem
{
	// Enums
	public int AudioVisualizationState;
	public int ObjectAmount;
	public int CurChannel;

	// Material Settings
	public float EmissiveColorStrength;
	public float EmissiveExposureStrength;
	public SerializableColor BaseColor;

	// Cluster Settings
	public SerializableVector3 Offset;
	public SerializableVector3 ClusterRotation;
	public SerializableVector3 AmplitudeScaler;
	public SerializableVector3 MinAddedScale;
	public float Spacing;

	// Item Name
	public string ItemName;
	public string Imagepath;

	// Single Object Settings
	public SerializableVector3 ObjectMinHeight;
	public SerializableVector3 ObjectScale;
	public SerializableVector3 ObjectRotation;
	public float BufferDecreaseDefault;
	public float BufferDecreaseFactor;

	public SaveItem(AudioItem item, string imagepath)
	{
		// Enums
		AudioVisualizationState = (int)item.AudioVisualizationState;
		ObjectAmount = (int)item.ObjectAmount;
		CurChannel = (int)item.CurChannel;

		// Material Settings
		EmissiveColorStrength = item.EmissiveColorStrength;
		EmissiveExposureStrength = item.EmissiveExposureStrength;
		BaseColor = new SerializableColor(item.BaseColor);

		// Cluster Settings
		Offset = new SerializableVector3(item.Offset);
		ClusterRotation = new SerializableVector3(item.ClusterRotation);
		AmplitudeScaler = new SerializableVector3(item.AmplitudeScaler);
		MinAddedScale = new SerializableVector3(item.MinAddedScale);
		Spacing = item.Spacing;

		// Item Name
		ItemName = item.ItemName;
		Imagepath = imagepath;

		// Single Object Settings
		ObjectMinHeight = new SerializableVector3(item.ObjectMinHeight);
		ObjectScale = new SerializableVector3(item.ObjectScale);
		ObjectRotation = new SerializableVector3(item.ObjectRotation);
		BufferDecreaseDefault = item.BufferDecreaseDefault;
		BufferDecreaseFactor = item.BufferDecreaseFactor;
	}
}
#endregion
#region Serializable Structs
[System.Serializable]
public struct SerializableVector3
{
	public float x, y, z;

	public SerializableVector3(Vector3 v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
	}

	public Vector3 ToVector3()
	{
		return new Vector3(x, y, z);
	}
}
[System.Serializable]
public struct SerializableColor
{
	float r, g, b, a;

	public SerializableColor(Color c)
	{
		r = c.r;
		g = c.g;
		b = c.b;
		a = c.a;
	}

	public Color ToColor()
	{
		return new Color(r, g, b, a);
	}
}
#endregion