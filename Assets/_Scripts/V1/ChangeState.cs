﻿using UnityEngine;
using TMPro;

public class ChangeState : MonoBehaviour
{
	public enum PlayerStates { Idle, Dead, Damaged, Fear, Run, Anger, Whatever }
	private PlayerStates currentPlayerState;
	private PlayerStates oldPlayerState;
	public PlayerStates defaultState;

	public AudioClip defaultClip;
	[SerializeField] private TextMeshProUGUI stateText;
	[SerializeField] private AudioSource source;
	private AudioClip clip;

	void Start()
	{
		currentPlayerState = PlayerStates.Idle;
		defaultState = PlayerStates.Idle;
		stateText.text = currentPlayerState.ToString();
		source = GameObject.Find("Main Camera").GetComponent<AudioSource>();
		clip = defaultClip;
	}

	void Update()
	{
		// return if the states are the same
		if (currentPlayerState == oldPlayerState)
			return;

		// switch if the states vary
		//switch (currentPlayerState)
		//{
		//	case PlayerStates.Idle:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Dead:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Damaged:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Fear:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Run:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Anger:
		//		source.clip = clip;
		//		break;
		//	case PlayerStates.Whatever:
		//		source.clip = clip;
		//		break;
		//	default:
		//		break;
		//}

		source.clip = clip;			// placeholder because in this case we only use audio
		source.Play();

		oldPlayerState = currentPlayerState;
	}

	public void OnStateChange(PlayerStates playerState, AudioClip audioClip)
	{
		// CHANGE STATE
		currentPlayerState = playerState;
		clip = audioClip;
		stateText.text = currentPlayerState.ToString();
		// BLEND MUSIC
	}
}
