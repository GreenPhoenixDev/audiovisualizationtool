﻿using UnityEngine;

public class MovePlayer : MonoBehaviour
{
	[SerializeField] private float speed;

    void Update()
    {
		float horizontal = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
		float vertical = Input.GetAxis("Vertical") * speed * Time.deltaTime;

		transform.position += new Vector3(horizontal, 0f, vertical);
    }
}
