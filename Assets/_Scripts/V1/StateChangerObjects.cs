﻿using UnityEngine;

public class StateChangerObjects : MonoBehaviour
{
	[SerializeField] private AudioClip clip;
	[SerializeField] private ChangeState.PlayerStates changeStateState;
	[SerializeField] private ChangeState changeState;

	void OnTriggerEnter(Collider other)
	{
		changeState.OnStateChange(changeStateState, clip);
	}

	void OnTriggerExit(Collider other)
	{
		changeState.OnStateChange(changeState.defaultState, changeState.defaultClip);
	}
}
