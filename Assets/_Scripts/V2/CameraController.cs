﻿using UnityEngine;

// this is the unity intern KeyboardOrbit / with a few changes
public class CameraController : MonoBehaviour
{
	[SerializeField] private Transform target;
	[SerializeField] private Vector3 targetOffset;
	[SerializeField] private float distance = 20.0f;
	[SerializeField] private float minDistance = 1f;
	[SerializeField] private float maxDistance = 1000f;
	[SerializeField] private float scrollSpeed = 2.0f;

	[SerializeField] private float xSpeed = 100f;
	[SerializeField] private float ySpeed = 100f;

	[SerializeField] private int yMinLimit = -723;
	[SerializeField] private int yMaxLimit = 877;

	private float x = 0.0f;
	private float y = 0.0f;

	public void Start()
	{
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
	}

	public void LateUpdate()
	{
		if (target)
		{
			x -= Input.GetAxis("Horizontal") * xSpeed * 0.02f;
			y += Input.GetAxis("Vertical") * ySpeed * 0.02f;

			y = ClampAngle(y, yMinLimit, yMaxLimit);

			distance -= Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;

			if (distance < minDistance)
				distance = minDistance;
			if (distance > maxDistance)
				distance = maxDistance;

			Quaternion rotation = Quaternion.Euler(y, x, 0.0f);
			Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position + targetOffset;

			transform.rotation = rotation;
			transform.position = position;
		}
	}

	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360.0f)
			angle += 360.0f;
		if (angle > 360.0f)
			angle -= 360.0f;
		return Mathf.Clamp(angle, min, max);
	}
}