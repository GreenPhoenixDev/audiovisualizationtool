﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V2;

public class DiscoLight : MonoBehaviour
{
	[SerializeField] private GameObject discoSphere;
	[SerializeField] private float colorChangeTime = 1f;
	[SerializeField] private float colorStepBase = 0.025f;
	[SerializeField] private float minIntensity = 0.2f;
	[SerializeField] private float maxIntensity = 5f;
	[SerializeField] private float emissiveIntensity;

	private Light discoLight;
	private Material discoSphereMaterial;
	private AudioVisualizationItem aVI;
	private Color[] colors = new Color[7];
	private Color lerpedColor = Color.red;
	private float colorStep;
	private int i;


	void Start()
	{
		discoLight = GetComponent<Light>();
		discoSphereMaterial = discoSphere.GetComponent<MeshRenderer>().material;

		colors[0] = Color.red;
		colors[1] = Color.yellow;
		colors[2] = Color.green;
		colors[3] = Color.cyan;
		colors[4] = Color.blue;
		colors[5] = Color.magenta;
		colors[6] = colors[0];
	}

	void Update()
    {
		if(colorStep < colorChangeTime)
		{
			lerpedColor = Color.Lerp(colors[i], colors[i + 1], colorStep);
			discoLight.color = lerpedColor;
			colorStep += colorStepBase;
		}
		else
		{
			colorStep = 0f;

			if(i < (colors.Length - 2))
			{
				i++;
			}
			else
			{
				i = 0;
			}
		}

		discoSphereMaterial.SetColor("_BaseColor", lerpedColor);
		discoSphereMaterial.SetColor("_EmissiveColor", lerpedColor);
		discoSphereMaterial.SetFloat("_EmissiveIntensity", emissiveIntensity);
		//discoLight.intensity = (aVI.audioBandBuffer[1] * (maxIntensity - minIntensity)) + minIntensity;
    }
}
