﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioItem : MonoBehaviour
{
	#region References
	[Space]
	public AudioItemListUIItem AILUII;
	SampleRateManager sRM;
	#endregion
	#region Enums
	public VisualizationState AudioVisualizationState = VisualizationState.FrequencyBands;
	public FactorsOfTwo ObjectAmount = FactorsOfTwo._8;
	public Channel CurChannel = Channel.Stereo;
	#endregion
	#region Private Variables
	List<float> freqBand = new List<float>();
	List<float> bandBuffer = new List<float>();
	List<float> bufferDecrease = new List<float>();
	List<float> freqBandHighest = new List<float>();
	List<float> audioBand = new List<float>();
	List<float> audioBandBuffer = new List<float>();
	List<Material> materials = new List<Material>();

	Transform obj;
	AnimationCurve sampleCurve;

	Matrix4x4 matrix;
	LayerMask layer;

	int objCount;
	float amplitudeHighest;
	string imagepath;
	#endregion
	#region Public Variables / Properties
	[HideInInspector] public string ItemKey { get; set; }

	[Header("Mesh")]
	public Mesh ObjMesh;

	[Space]
	[Header("Material Settings")]
	public Material Mat;
	[Range(0f, Mathf.Infinity)] public float EmissiveColorStrength = 1f;
	[Range(0f, 1f)] public float EmissiveExposureStrength = 1f;
	public Color BaseColor = Color.white;

	[Space]
	[Header("Cluster Settings")]
	public Vector3 Offset;
	public Vector3 ClusterRotation;
	public Vector3 AmplitudeScaler;
	public Vector3 MinAddedScale;
	public float Spacing = 1f;
	public float Amplitude;
	public float AmplitudeBuffer;

	[Space]
	[Header("Item Name")]
	public string ItemName = "New Item";

	[Space]
	[Header("Single Object Settings")]
	public Vector3 ObjectMinHeight;
	public Vector3 ObjectScale;
	public Vector3 ObjectRotation;
	[Range(0.0001f, 0.02f)] public float BufferDecreaseDefault = 0.005f;
	[Range(1f, 1.5f)] public float BufferDecreaseFactor = 1.2f;
	#endregion

	#region Initialization
	public void InitItem(SampleRateManager _sRM)
	{
		sRM = _sRM;

		// set the default layer
		layer = LayerMask.NameToLayer("Default");

		// init int values
		objCount = Utility.SwitchFactorsOfTwoFull(ObjectAmount);

		// init object transform
		GameObject objGO = new GameObject();
		objGO.hideFlags = HideFlags.HideInHierarchy;
		obj = objGO.transform;
		obj.localScale = new Vector3(
			ObjectMinHeight.x,
			ObjectMinHeight.y,
			ObjectMinHeight.z
			);

		ChangeObjectCount();
	}
	#endregion
	#region MonoBehaviour
	void Update()
	{
		FrequencyBands();
		BandBuffer();
		AudioBands();
		SetScales();

		// DOES NOT EFFECT THE CUBES BECAUSE THEY ARE DRAWN
		//GetAmplitude();
	}
	#endregion
	#region Updated
	void FrequencyBands()
	{
		int count = 0;
		for (int i = 0; i < freqBand.Count; i++)
		{
			float average = 0;
			int sampleCount = (int)sampleCurve.Evaluate(i);

			for (int j = 0; j < sampleCount; j++)
			{
				switch (CurChannel)
				{
					case Channel.Stereo:
						average += (sRM.samplesLeft[count] + sRM.samplesRight[count]) * (count + 1);
						break;
					case Channel.Left:
						average += sRM.samplesLeft[count] * (count + 1);
						break;
					case Channel.Right:
						average += sRM.samplesRight[count] * (count + 1);
						break;
					default:
						break;
				}
				count++;
			}

			average /= count;

			freqBand[i] = average /** freqBandBufferScaler*/;
		}
	}
	void BandBuffer()
	{
		for (int i = 0; i < bandBuffer.Count; i++)
		{
			if (freqBand[i] > bandBuffer[i])
			{
				bandBuffer[i] = freqBand[i];
				bufferDecrease[i] = BufferDecreaseDefault;
			}
			else if (freqBand[i] <= bandBuffer[i])
			{
				bandBuffer[i] -= bufferDecrease[i];
				bufferDecrease[i] *= BufferDecreaseFactor;
			}
		}
	}
	void AudioBands()
	{
		for (int i = 0; i < audioBand.Count; i++)
		{
			if (freqBand[i] > freqBandHighest[i])
			{
				freqBandHighest[i] = freqBand[i];
			}
			audioBand[i] = (freqBand[i] / freqBandHighest[i]);
			audioBandBuffer[i] = (bandBuffer[i] / freqBandHighest[i]);
		}
	}
	void GetAmplitude()
	{
		float curAmp = 0f;
		float curAmpBuffer = 0f;

		for (int i = 0; i < objCount; i++)
		{
			curAmp += audioBand[i];
			curAmpBuffer += audioBandBuffer[i];
		}

		if(curAmp > amplitudeHighest) { amplitudeHighest = curAmp; }

		Amplitude = curAmp / amplitudeHighest;
		AmplitudeBuffer = curAmpBuffer / amplitudeHighest;

		transform.localScale = new Vector3(
			Amplitude * AmplitudeScaler.x + MinAddedScale.x,
			Amplitude * AmplitudeScaler.y + MinAddedScale.y,
			Amplitude * AmplitudeScaler.z + MinAddedScale.z
			);
	}
	void SetScales()
	{
		for (int i = 0; i < objCount; i++)
		{
			Color color = new Color();

			switch (AudioVisualizationState)
			{
				case VisualizationState.FrequencyBands:
					obj.localScale = new Vector3(
						Mathf.Clamp(ObjectMinHeight.x + freqBand[i] * ObjectScale.x, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.y + freqBand[i] * ObjectScale.y, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.z + freqBand[i] * ObjectScale.z, 0f, Mathf.Infinity)
						);
					color = new Color(audioBand[i], audioBand[i], audioBand[i]);
					SetColors(i, audioBand[i], color);
					break;
				case VisualizationState.BandBuffer:
					obj.localScale = new Vector3(
						Mathf.Clamp(ObjectMinHeight.x + bandBuffer[i] * ObjectScale.x, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.y + bandBuffer[i] * ObjectScale.y, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.z + bandBuffer[i] * ObjectScale.z, 0f, Mathf.Infinity)
						);
					color = new Color(audioBandBuffer[i], audioBandBuffer[i], audioBandBuffer[i]);
					SetColors(i, audioBand[i], color);
					break;
				case VisualizationState.AudioBands:
					obj.localScale = new Vector3(
						Mathf.Clamp(ObjectMinHeight.x + audioBand[i] * ObjectScale.x, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.y + audioBand[i] * ObjectScale.y, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.z + audioBand[i] * ObjectScale.z, 0f, Mathf.Infinity)
						);
					color = new Color(audioBand[i], audioBand[i], audioBand[i]);
					SetColors(i, audioBand[i], color);
					break;
				case VisualizationState.AudioBandBuffer:
					obj.localScale = new Vector3(
						Mathf.Clamp(ObjectMinHeight.x + audioBandBuffer[i] * ObjectScale.x, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.y + audioBandBuffer[i] * ObjectScale.y, 0f, Mathf.Infinity),
						Mathf.Clamp(ObjectMinHeight.z + audioBandBuffer[i] * ObjectScale.z, 0f, Mathf.Infinity)
						);
					color = new Color(audioBandBuffer[i], audioBandBuffer[i], audioBandBuffer[i]);
					SetColors(i, audioBand[i], color);
					break;
				default:
					break;
			}

			DrawMeshes(obj, i);
		}
	}
	void SetColors(int i, float factor, Color emissiveColor)
	{
		materials[i].SetColor("_EmissiveColor", emissiveColor * EmissiveColorStrength);
		materials[i].SetFloat("_EmissiveExposureWeight", factor * EmissiveExposureStrength);
	}
	void DrawMeshes(Transform obj, int i)
	{
		transform.rotation = Quaternion.Euler(ClusterRotation.x, ClusterRotation.y, ClusterRotation.z);

		//Vector3 newOffset = new Vector3(i * Spacing, 0f, 0f) + Offset;
		Vector3 newPos = transform.forward * i * Spacing + Offset;
		Quaternion newRot = Quaternion.Euler(ObjectRotation.x, ObjectRotation.y, ObjectRotation.z)/* * singleRotation * Curve.Evaluate((mapped between 0-1)) */;

		matrix = Matrix4x4.TRS(newPos, newRot, obj.localScale);

		Graphics.DrawMesh(ObjMesh, matrix, materials[i], layer);
	}
	#endregion
	#region Change Variables
	public void ChangeObjectCount()
	{
		// update the object count
		objCount = Utility.SwitchFactorsOfTwoFull(ObjectAmount);

		// clear Lists
		freqBand.Clear();
		bandBuffer.Clear();
		bufferDecrease.Clear();
		freqBandHighest.Clear();
		audioBand.Clear();
		audioBandBuffer.Clear();
		materials.Clear();

		// update Lists
		for (int i = 0; i < objCount; i++)
		{
			freqBand.Add(0f);
			bandBuffer.Add(0f);
			bufferDecrease.Add(0f);
			freqBandHighest.Add(0f);
			audioBand.Add(0f);
			audioBandBuffer.Add(0f);
			Material newMat = new Material(Mat.shader);
			newMat.CopyPropertiesFromMaterial(Mat);
			materials.Add(newMat);
			materials[i].SetColor("_BaseColor", BaseColor);
		}

		// update the object curve
		sampleCurve = sRM.SwitchAnimationCurve(ObjectAmount);
	}
	public void ChangeBaseColor()
	{
		for (int i = 0; i < materials.Count; i++)
		{
			materials[i].SetColor("_BaseColor", BaseColor);
		}
	}
	public void OverrideValues(SaveItem save)
	{
		// Enums
		AudioVisualizationState = (VisualizationState)save.AudioVisualizationState;
		ObjectAmount = (FactorsOfTwo)save.ObjectAmount;
		CurChannel = (Channel)save.CurChannel;

		// Material Settings
		EmissiveColorStrength = save.EmissiveColorStrength;
		EmissiveExposureStrength = save.EmissiveExposureStrength;
		BaseColor = save.BaseColor.ToColor();
		ChangeBaseColor();

		// Cluster Settings
		Offset = save.Offset.ToVector3();
		ClusterRotation = save.ClusterRotation.ToVector3();
		AmplitudeScaler = save.AmplitudeScaler.ToVector3();
		MinAddedScale = save.MinAddedScale.ToVector3();
		Spacing = save.Spacing;

		// Item Name
		ItemName = save.ItemName;
		imagepath = save.Imagepath;

		// Single Object Settings
		ObjectMinHeight = save.ObjectMinHeight.ToVector3();
		ObjectScale = save.ObjectScale.ToVector3();
		ObjectRotation = save.ObjectRotation.ToVector3();
		BufferDecreaseDefault = save.BufferDecreaseDefault;
		BufferDecreaseFactor = save.BufferDecreaseFactor;
	}
	#endregion
}