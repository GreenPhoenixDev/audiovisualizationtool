﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioItemManager : MonoBehaviour
{
	#region References
	SampleRateManager sRM;
	AudioItemListUI aILUI;
	#endregion
	#region Private Variables
	public Dictionary<string, AudioItem> ItemDict = new Dictionary<string, AudioItem>();

	[SerializeField] Transform itemParent;

	public const string DefaultName = "Item";
	#endregion
	#region Public Variables
	public GameObject ItemPrefab;
	#endregion

	#region MonoBehaviour
	void Start()
	{
		sRM = GetComponent<SampleRateManager>();
		aILUI = GetComponent<AudioItemListUI>();
	}
	#endregion
	#region Public Methods
	public void AddNewItem()
	{
		GameObject newItemGO = Instantiate(ItemPrefab, transform.position, transform.rotation, itemParent);
		AudioItem newItem = newItemGO.GetComponent<AudioItem>();

		// make sure the same name doesn't already exist
		int i = 0;
		while (ItemDict.ContainsKey(DefaultName + i.ToString()))
		{
			i++;
		}

		newItem.ItemKey = DefaultName + i.ToString();
		newItem.ItemName += i.ToString();
		newItem.InitItem(sRM);
		ItemDict.Add(newItem.ItemKey, newItem);

		aILUI.AddAudioItem(newItem);
	}
	public AudioItem AddOldItem()
	{
		GameObject newItemGO = Instantiate(ItemPrefab, transform.position, transform.rotation, itemParent);
		AudioItem newItem = newItemGO.GetComponent<AudioItem>();

		// make sure the same name doesn't already exist
		int i = 0;
		while (ItemDict.ContainsKey(DefaultName + i.ToString()))
		{
			i++;
		}

		newItem.ItemKey = DefaultName + i.ToString();
		newItem.ItemName += i.ToString();
		newItem.InitItem(sRM);
		ItemDict.Add(newItem.ItemKey, newItem);

		aILUI.AddAudioItem(newItem);

		return newItem;
	}
	public void DeleteItem(string key)
	{
		Destroy(ItemDict[key].gameObject);
		ItemDict.Remove(key);
	}
	public void ClearScene(AudioItemListUIItem[] items)
	{
		for (int i = 0; i < items.Length; i++)
		{
			items[i].DefinitelyDestroy();
		}
	}
	#endregion
}
