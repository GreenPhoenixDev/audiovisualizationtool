﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class PresetManager : MonoBehaviour
{
	#region References
	[SerializeField] ScreenRecorder recorder;
	[SerializeField] SaveAndLoad saveLoader;
	#endregion
	#region Private Variables
	[Space]
	[SerializeField] GameObject createPresetPanel;
	[SerializeField] GameObject presetUIItem;
	[SerializeField] Transform presetUIItemParent;
	[SerializeField] InputField nameInput;
	[SerializeField] TMP_Text errorText;

	Dictionary<string, string> presetDict = new Dictionary<string, string>();
	Dictionary<string, PresetUIItem> presetUIItemDict = new Dictionary<string, PresetUIItem>();

	string curName;
	bool isLoading;
	string imageFolder;
	#endregion

	#region MonoBehaviour
	void Start()
	{
		string[] presets = Directory.GetFiles(saveLoader.SavesPath);
		List<string> presetsList = new List<string>(presets);
		Dictionary<string, string> imagesDict = new Dictionary<string, string>();

		// if the screenshots folder does not exist, create or fill it
		if (imageFolder == null || imageFolder.Length == 0)
		{
			imageFolder = Application.persistentDataPath + "/" + recorder.ScreenShotFolder;

			Directory.CreateDirectory(imageFolder);
		}

		string[] images = Directory.GetFiles(imageFolder);
		List<string> imagesList = new List<string>(images);

		for (int i = images.Length - 1; i > -1; i--)
		{
			// remove *.meta files
			string[] lineMeta = images[i].Split('.');
			if (lineMeta[lineMeta.Length - 1] == "meta")
			{
				imagesList.Remove(images[i]);
				continue;
			}

			string[] parts = imagesList[i].Split('_');
			string[] parts2 = parts[parts.Length - 1].Split('.');
			string imageName = parts2[0];

			imagesDict.Add(imageName, imagesList[i]);
		}

		for (int i = presets.Length - 1; i > -1; i--)
		{
			// remove *.meta files
			string[] lineMeta = presetsList[i].Split('.');
			if (lineMeta[lineMeta.Length - 1] == "meta")
			{
				presetsList.Remove(presetsList[i]);
				continue;
			}

			// create UI item
			string[] parts = presetsList[i].Split('/');
			//string[] parts2 = parts[parts.Length - 1].Split('\\');
			string presetName = parts[parts.Length - 1];

			// create sprite
			StartCoroutine(LoadImageToPreset(imagesDict[presetName], presetName));

			presetDict.Add(presetName, presetName);

			// create PresetUIItem
			GameObject newUIItem = Instantiate(presetUIItem, presetUIItemParent);
			PresetUIItem presetItem = newUIItem.GetComponent<PresetUIItem>();
			presetItem.Init(saveLoader, presetName);

			presetUIItemDict.Add(presetName, presetItem);

			presetUIItemDict[presetName].FinishedLoading();
		}

	}
	#endregion
	#region Public Methods
	public void AskCreatePreset()
	{
		createPresetPanel.SetActive(true);
		errorText.text = "Give a name";
	}
	public void Cancel()
	{
		createPresetPanel.GetComponent<InputField>().text = "";
		createPresetPanel.SetActive(false);
	}
	public void CheckForPresetName()
	{
		if (nameInput.text == "")
		{
			errorText.text = "No name given!";
			return;
		}
		else if (presetDict.ContainsKey(nameInput.text))
		{
			errorText.text = "Name is already taken!";
			return;
		}
		else if (isLoading)
		{
			errorText.text = "A Preset is still loading";
			return;
		}

		errorText.text = "";

		curName = nameInput.text;

		recorder.TakeScreenShot = true;
		recorder.PresetName = curName;
		isLoading = true;
	}
	public void CreatePreset(string filename)
	{
		presetDict.Add(curName, curName);

		// Create PresetUIItem
		GameObject newUIItem = Instantiate(presetUIItem, presetUIItemParent);
		PresetUIItem presetItem = newUIItem.GetComponent<PresetUIItem>();
		presetItem.Init(saveLoader, curName);

		presetUIItemDict.Add(curName, presetItem);

		StartCoroutine(LoadImageToPresetAndSave(filename, curName));
	}
	#endregion

	#region Coroutines
	IEnumerator LoadImageToPresetAndSave(string imagepath, string key)
	{
		// Create Save File
		saveLoader.Save(presetDict[curName], imagepath);

		presetUIItemDict[key].FinishedLoading();

		isLoading = false;

		WWW www = new WWW(imagepath);

		Debug.LogError($"imagepath: {imagepath}");
		yield return www;
		Debug.LogError("reached savepoint");
		Sprite newSprite = Sprite.Create(www.texture, new Rect(0f, 0f, recorder.CaptureWidth, recorder.CaptureHeight), new Vector2(0f, 0f), 100f);

		presetUIItemDict[key].FinishedLoadingSprite(newSprite);
	}
	IEnumerator LoadImageToPreset(string filename, string key)
	{
		WWW www = new WWW(filename);

		yield return www;

		Sprite newSprite = Sprite.Create(www.texture, new Rect(0f, 0f, recorder.CaptureWidth, recorder.CaptureHeight), new Vector2(0f, 0f), 100f);

		presetUIItemDict[key].FinishedLoading();
		presetUIItemDict[key].FinishedLoadingSprite(newSprite);
	}
	#endregion
}