﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PresetUIItem : MonoBehaviour
{
	#region References
	SaveAndLoad saveLoader;
	#endregion
	#region Private Variables
	[SerializeField] Image screenShot;
	[SerializeField] TMP_Text displayMessage;

	string presetName;
	#endregion

	#region Public Methods
	public void Init(SaveAndLoad _saveLoader, string name)
	{
		displayMessage.text = "Loading...";
		presetName = name;
		saveLoader = _saveLoader;
	}
	public void FinishedLoading()
	{
		displayMessage.text = presetName;
	}
	public void FinishedLoadingSprite(Sprite sprite)
	{
		screenShot.sprite = sprite;
	}
	public void LoadPreset()
	{
		saveLoader.Load(presetName);
	}
	#endregion
}