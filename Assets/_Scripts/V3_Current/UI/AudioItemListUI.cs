﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class AudioItemListUI : MonoBehaviour
{
	#region References
	[SerializeField] CurrentItemUI cIUI;
	AudioItemManager aIM;
	#endregion
	#region Private Variables
	[SerializeField] GameObject itemUI;
	[SerializeField] GameObject askToDestroyUI;
	[SerializeField] Transform itemUIParent;
	[SerializeField] TMP_Text askToDestroyUIText;

	string itemKey;
	#endregion
	#region Public Variables
	AudioItem currentItem;
	public AudioItem CurrentItem { get => currentItem; set => currentItem = value; }
	public Dictionary<string, GameObject> ItemDictUI = new Dictionary<string, GameObject>();
	#endregion

	#region MonoBehaviour
	private void Start()
	{
		aIM = GetComponent<AudioItemManager>();
	}
	#endregion

	#region Handle Items
	public void AddAudioItem(AudioItem item)
	{
		var newItemUI = Instantiate(itemUI, itemUIParent);
		ItemDictUI.Add(item.ItemKey, newItemUI);

		AudioItemListUIItem uiItem = newItemUI.GetComponent<AudioItemListUIItem>();
		item.AILUII = uiItem;
		uiItem.Init(item, this, cIUI);

		currentItem = item;
		cIUI.SetValues(item);
	}
	public void AskToDestroy(AudioItemListUIItem itemUIItem)
	{
		askToDestroyUI.SetActive(true);
		askToDestroyUIText.text = itemUIItem.Item.ItemName;
		itemKey = itemUIItem.Item.ItemKey;
	}
	public void CloseAskToDestroy() { askToDestroyUI.SetActive(false); }
	public void SetItemKey(string key) { itemKey = key; }
	public void RemoveAudioItem()
	{
		Destroy(ItemDictUI[itemKey].transform.gameObject);
		ItemDictUI.Remove(itemKey);
		askToDestroyUI.SetActive(false);
		aIM.DeleteItem(itemKey);

		// get a new random item when the current item is deleted
		if (ItemDictUI.Count == 0)
		{
			cIUI.SetNullValues();
		}
		else
		{
			currentItem = ItemDictUI.ElementAt(Random.Range(0, ItemDictUI.Count - 1)).Value.GetComponent<AudioItem>();
		}
	}
	public void SelectCurrentItem(AudioItem item) { currentItem = item; cIUI.SetValues(item); }
	#endregion
	#region Edit Items
	// Material Settings
	public void ChangeEmissiveColorStrength(InputField field) { currentItem.EmissiveColorStrength = float.Parse(field.text); }
	public void ChangeEmissiveExposureStrength(InputField field) { currentItem.EmissiveExposureStrength = float.Parse(field.text); }
	public void ChangeColorR(Slider slider)
	{
		currentItem.BaseColor.r = (slider.value / 255f);
		currentItem.ChangeBaseColor();
		cIUI.ChangeTextR();
		cIUI.ChangeColorProperties(currentItem.BaseColor);
	}
	public void ChangeColorG(Slider slider)
	{
		currentItem.BaseColor.g = (slider.value / 255f);
		currentItem.ChangeBaseColor();
		cIUI.ChangeTextG();
		cIUI.ChangeColorProperties(currentItem.BaseColor);
	}
	public void ChangeColorB(Slider slider)
	{
		currentItem.BaseColor.b = (slider.value / 255f);
		currentItem.ChangeBaseColor();
		cIUI.ChangeTextB();
		cIUI.ChangeColorProperties(currentItem.BaseColor);
	}

	// Cluster Settings
	public void ChangeOffsetX(InputField field) { currentItem.Offset.x = float.Parse(field.text); }
	public void ChangeOffsetY(InputField field) { currentItem.Offset.y = float.Parse(field.text); }
	public void ChangeOffsetZ(InputField field) { currentItem.Offset.z = float.Parse(field.text); }
	public void ChangeClusterRotationX(InputField field) { currentItem.ClusterRotation.x = float.Parse(field.text); }
	public void ChangeClusterRotationY(InputField field) { currentItem.ClusterRotation.y = float.Parse(field.text); }
	public void ChangeClusterRotationZ(InputField field) { currentItem.ClusterRotation.z = float.Parse(field.text); }
	public void ChangeClusterSpacing(InputField field) { currentItem.Spacing = float.Parse(field.text); }
	public void ChangeVisualizationState(int idx) { currentItem.AudioVisualizationState = (VisualizationState)idx; }
	public void ChangeObjectAmount(int idx) { currentItem.ObjectAmount = (FactorsOfTwo)idx; currentItem.ChangeObjectCount(); }
	public void ChangeChannel(int idx) { currentItem.CurChannel = (Channel)idx; }

	// Object Settings
	public void ChangeObjectMinHeightX(InputField field) { currentItem.ObjectMinHeight.x = float.Parse(field.text); }
	public void ChangeObjectMinHeightY(InputField field) { currentItem.ObjectMinHeight.y = float.Parse(field.text); }
	public void ChangeObjectMinHeightZ(InputField field) { currentItem.ObjectMinHeight.z = float.Parse(field.text); }
	public void ChangeObjectScaleX(InputField field) { currentItem.ObjectScale.x = float.Parse(field.text); }
	public void ChangeObjectScaleY(InputField field) { currentItem.ObjectScale.y = float.Parse(field.text); }
	public void ChangeObjectScaleZ(InputField field) { currentItem.ObjectScale.z = float.Parse(field.text); }
	public void ChangeObjectRotationX(InputField field) { currentItem.ObjectRotation.x = float.Parse(field.text); }
	public void ChangeObjectRotationY(InputField field) { currentItem.ObjectRotation.y = float.Parse(field.text); }
	public void ChangeObjectRotationZ(InputField field) { currentItem.ObjectRotation.z = float.Parse(field.text); }
	public void ChangeBufferDecreaseDefault(InputField field) { currentItem.BufferDecreaseDefault = float.Parse(field.text); }
	public void ChangeBufferDecreaseFactor(InputField field) { currentItem.BufferDecreaseFactor = float.Parse(field.text); }
	#endregion
}
