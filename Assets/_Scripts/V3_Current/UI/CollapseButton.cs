﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollapseButton : MonoBehaviour
{
	public RectTransform Rect;
	[HideInInspector] public bool IsCollapsed;
}
