﻿using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class DropDownHandler : MonoBehaviour
{
	#region Private Variables
	public TMP_Dropdown VisualizationStateDD;
	public TMP_Dropdown ObjectAmountDD;
	public TMP_Dropdown ChannelDD;
	public TMP_Dropdown ScreenSizeDD;
	public TMP_Dropdown ScreenModeDD;
	#endregion

	#region MonoBehaviour
	void Awake()
	{
		if (VisualizationStateDD != null)
		{
			// VSDD
			string[] vsEnumNames = Enum.GetNames(typeof(VisualizationState));
			List<string> vsNames = new List<string>(vsEnumNames);
			VisualizationStateDD.AddOptions(vsNames);
		}

		if (ObjectAmountDD != null)
		{
			// OADD
			string[] oaEnumNames = Enum.GetNames(typeof(FactorsOfTwo));
			List<string> oaNames = new List<string>();
			for (int i = 0; i < oaEnumNames.Length; i++)
			{
				string[] parts = oaEnumNames[i].Split('_');
				oaNames.Add(parts[parts.Length - 1]);
			}
			ObjectAmountDD.AddOptions(oaNames);
		}

		if (ChannelDD != null)
		{
			// CDD
			string[] cEnumNames = Enum.GetNames(typeof(Channel));
			List<string> cNames = new List<string>(cEnumNames);
			ChannelDD.AddOptions(cNames);
		}

		if (ScreenSizeDD != null)
		{
			// SSDD
			string[] ssEnumNames = Enum.GetNames(typeof(ScreenSize));
			List<string> ssNames = new List<string>();
			for (int i = 0; i < ssEnumNames.Length; i++)
			{
				string[] parts = ssEnumNames[i].Split('_');
				ssNames.Add(parts[parts.Length - 1]);
			}
			ScreenSizeDD.AddOptions(ssNames);
		}

		if (ScreenModeDD != null)
		{
			// SMDD
			string[] smEnumNames = Enum.GetNames(typeof(ScreenMode));
			List<string> smNames = new List<string>(smEnumNames);
			ScreenModeDD.AddOptions(smNames);
		}
	}
	#endregion
}
