﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
	#region Private variables
	ScreenMode curMode = ScreenMode.FullScreen;
	ScreenSize size = ScreenSize._1280x720;

	[SerializeField] GameObject menu;
	[SerializeField] TMP_Text volumeText;

	DropDownHandler ddh;
	#endregion

	#region MonoBehaviour
	void Awake()
	{
		// start with fullscreen in the build
		if (!Application.isEditor)
		{
			Screen.fullScreen = false;
			Resolution res = Screen.currentResolution;
			Screen.SetResolution(res.width, res.height, true);
		}
		ddh = GetComponent<DropDownHandler>();

		// load the saved prefs
		LoadSettingsPrefs();
	}
	void Update()
	{
		// activate and deactivate the menu
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (menu.activeSelf) { menu.SetActive(false); }
			else { menu.SetActive(true); }
		}

		// REMOVE AFTER TESTING
		if (Input.GetKeyDown(KeyCode.H))
		{
			Debug.developerConsoleVisible = !Debug.developerConsoleVisible;
		}
	}
	#endregion
	#region Public Methods
	// Quit
	public void QuitApplication()
	{
		SaveSettingsPrefs();
		Application.Quit();
	}

	// Change Window Settings
	public void ChangeScreenSize(int idx)
	{
		switch (curMode)
		{
			case ScreenMode.FullScreen:
				ddh.ScreenSizeDD.value = (int)size;
				break;
			case ScreenMode.Windowed:
				ScreenSize newSize = (ScreenSize)idx;

				if (Resize(newSize)) { size = (ScreenSize)idx; }
				break;
			default:
				break;
		}
	}
	public void ChangeScreenMode(int idx)
	{
		curMode = (ScreenMode)idx;

		switch (curMode)
		{
			case ScreenMode.FullScreen:
				Screen.fullScreen = false;
				Resolution res = Screen.currentResolution;
				Screen.SetResolution(res.width, res.height, true);
				break;
			case ScreenMode.Windowed:
				Screen.fullScreen = false;
				Resize(size);
				break;
			default:
				break;
		}
	}

	// Change Volume
	public void ChangeVolumeText(Slider slider)
	{
		volumeText.text = (slider.value * 100f).ToString("0.00");
	}

	// Change The Background Image
	public void ChangeBackgroundImage()
	{

	}
	#endregion
	#region Private Methods
	void SaveSettingsPrefs()
	{
	}
	void LoadSettingsPrefs()
	{
	}
	bool Resize(ScreenSize newSize)
	{
		// get the selected resolution
		string[] name = newSize.ToString().Split('_');
		string[] sizes = name[name.Length - 1].Split('x');
		int width = int.Parse(sizes[0]);
		int height = int.Parse(sizes[1]);

		Resolution res = Screen.currentResolution;

		// if it does not, return
		if (width > res.width || height > res.height)
		{
			ddh.ScreenSizeDD.value = (int)size;

			return false;
		}

		Screen.SetResolution(width, height, false);

		return true;
	}
	#endregion
}
