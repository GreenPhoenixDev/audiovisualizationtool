# Audio Visualizer

In this project I want to build a vizualizer for audio files.

This is not supposed to be a game,  but I wanted to create an environment, that is moving accordingly to the audio, that is playing at the time.
So in this project I just want to mess around lot and try it in a very generic way at some point. 
This is way more theoretical than I would normally like it, but it is definitely a good experience.

So far it is possible to create items and change their values at realtime. 
For this to happen I had to change from editing GameObjects, to just draw the meshes at realtime, which is way more easy to accomplish, than changing GameObjects and 
editing them when checking for changes.
You can save and reload presets that you created and drag in audio files(only .wav at the moment) to create a playlist.

#### Game View

![](https://imgur.com/ud6mMaU.png "With UI")
![](https://imgur.com/KJVKiTn.png "Without UI")
![](https://imgur.com/G7mQqUK.png "General Settings")
![](https://imgur.com/s2XPP76.png "Presets")

In the game view you get to Play, Pause, Stop, Skip a song or go back to one.
On the right of the screen get a list of all the items, that are currently in the scene.
You are able to delete the items when pressing the red button next to their name in the list. 
On the left side are the editable properties of the currently selected item.

#### Editor View

| Editor View Of Audio Item | Extra Dropdowns |
| --- | ---: |
| ![](https://imgur.com/mDIGmQe.png) | ![](https://imgur.com/VIh2hza.png) ![](https://imgur.com/fDsI1u5.png) ![](https://imgur.com/3wgUJv2.png) |

In the left cell are the editable properties of an audio item and in the right the dropdowns for the way the meshes move to the audio, the amount of meshes that 
should be rendered and the channel of the audio that a script listenes to.
